# Image Management

Small Image Management platform example.

Built with Django2 + Reactjs

A small web site using the Django Framework backend. You
should use at least 5 random pictures (min 500x500px) on the platform.

###Database shema :
- Image
- Upload Timestamp
- Verified Flag => True/False
- Rejected Flag => True/False

###Content :
- Home Page
- Image Upload Page => A form where you can upload a selected image on your
computer. The image should be a .jpeg or a .png
- Image List => List all the images stored in the database with their flags
- Image viewer
 
    - Show one image in full page
    - browse using the keyboard arrows
    - Press “X” to reject the picture and go to the next one
    - Press “P” to keep the picture and go to the next one
    - If the image is Kept or Rejected the state of the image should be set to
verified=True.

###Environment :
- Python 3+
- Django Framework. 
- SQLite database
- Modern Browser 
- For the front end : Reactjs

###Execution :
- Frontend (dev mode): ``` npm run watch ```
- Backend with Django : ``` python3 manage.py runserver ```
- Access : [http://localhost:8000/](http://localhost:8000/)
