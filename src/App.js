import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import ImageList from './imagecomponent';
import ImageGallery from 'react-image-gallery';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {library} from '@fortawesome/fontawesome-svg-core'
import {faCheckCircle, faTimesCircle} from '@fortawesome/free-solid-svg-icons'

library.add(faCheckCircle, faTimesCircle);

import "react-image-gallery/styles/css/image-gallery.css";


const client = require('./client');
const root = 'http://localhost:8000/api/';

function getCookie(name) {
    function escape(s) {
        return s.replace(/([.*+?\^${}()|\[\]\/\\])/g, '\\$1');
    };
    var match = document.cookie.match(RegExp('(?:^|;\\s*)' + escape(name) + '=([^;]*)'));
    return match ? match[1] : '';
}

const CSRFToken = () => {
    return (
        <input type="hidden" name="csrfmiddlewaretoken" value={getCookie('csrftoken')}/>
    );
};

class CreateDialog extends Component {

    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(e) {
        e.preventDefault();

        var formdata = new FormData(ReactDOM.findDOMNode(this.refs.form_images_upload));
        this.props.onCreate(formdata);
        ReactDOM.findDOMNode(this.refs.images_upload).value = '';
        // Navigate away from the dialog to hide it.
        window.location = "#";
    }

    render() {
        return (
            <div>

                <h2>Upload new Image</h2>

                <form ref="form_images_upload" className="form-inline" encType="multipart/form-data">
                    <CSRFToken/>
                    <div className="form-group">
                        <label htmlFor="images_upload" className="mx-2"> Images </label>
                        <input type="file" ref="images_upload" name="image" accept=".jpeg, .png"/>
                    </div>
                    <div className="invisible">
                        <input name="verified" type="checkbox" value="false"/>
                        <input name="rejected" type="checkbox" value="false"/>
                    </div>
                    <button onClick={this.handleSubmit} className="btn btn-primary btn-sm ">Submit</button>
                </form>
            </div>
        )
    }

}

class App extends Component {
    constructor(props) {
        super(props);
        this.onCreate = this.onCreate.bind(this);
        this.state = {images: [], currentPage: 1, nbPage: 1, count: 0, gallery: []};
        this.onCreate = this.onCreate.bind(this);
        this.onUpdate = this.onUpdate.bind(this);
        this.handleKeyDown = this.handleKeyDown.bind(this);
        this.onDelete = this.onDelete.bind(this);
        this.onNavigate = this.onNavigate.bind(this);
        this.setIndexViewer = this.setIndexViewer.bind(this);
    }

    loadFromServer(currentPage) {
        let url = currentPage == 1 ? root + 'images/' : root + "images/?page=" + currentPage;
        client({path: url}).then(img => {
            client({path: root + 'gallery/'}).then(res => {
                this.setState({
                    images: img.entity.results,
                    currentPage: currentPage,
                    count: img.entity.count,
                    gallery: res.entity,
                    nbPage: img.entity.count % 2 === 0 ? img.entity.count / 2 : Math.trunc(img.entity.count / 2) + 1
                });
            })
        });
    }

    onCreate(images) {
        client({
            method: 'POST',
            path: root + 'images/',
            entity: images,
            headers: {'Content-Type': 'multipart/form-data'}
        }).then(response => {
                this.onNavigate(this.state.count % 2 === 0 ? this.state.nbPage + 1 : this.state.nbPage)
            }, res => {
                if (res.status.code === 400) {
                    alert("Error:" + res.entity.image[0]);
                }
            }
        );
    }

    onUpdate(code) {
        let img = this.state.gallery[this._imageGallery.getCurrentIndex()];
        if(code === 2){
            // Reject the picture
            img.rejected = true;
        }
        img.verified = true;
        delete img.image;
        client({
			method: 'PATCH',
			path: `${root}gallery/${img.id}/`,
			entity: img,
			headers: {
				'Content-Type': 'application/json'
			}
		}).done(response => {
			this.loadFromServer(this.state.currentPage);
			this._imageGallery.slideToIndex(this._imageGallery.getCurrentIndex()%this.state.count+1);
		}, response => {
			if (response.status.code === 412) {
				alert('DENIED: Unable to update ');
			}
		});
    }

    onDelete(image) {

    }

    parseData(images) {
        var getName = (url) => {
            let u = new URL(url);
            return u.pathname.split("/").pop();
        };
        var getDate = (date) => {
            let d = new Date(date);
            return d.toString();
        };
        let img = [];
        images.map(res => {
            img.push({
                original: res.image,
                thumbnail: res.image,
                originalTitle: getName(res.image),
                thumbnailTitle: getName(res.image),
                description: getDate(res.timestamp),
                rejected: res.rejected,
                verified: res.verified,
            });
        })
        return img;
    }

    renderItem(item) {
        var check = (val) => {
            return val ? <FontAwesomeIcon icon="check-circle" color="green"/> :
                <FontAwesomeIcon icon="times-circle" color="red"/>;
        };
        return (
            <div className='image-gallery-image'>
                {
                    <img
                        src={item.original}
                        alt={item.originalAlt}
                        srcSet={item.srcSet}
                        sizes={item.sizes}
                        title={item.originalTitle}
                    />

                }
                <div className="image-gallery-flag">
                    <p>Verified {check(item.verified)}</p>
                    <p>Rejected {check(item.rejected)}</p>
                </div>
                {
                    item.description &&
                    <span className='image-gallery-description'>
                        {item.description}
                    </span>
                }
            </div>
        );
    }

    setIndexViewer(index) {
        let indexMap = new Map();
        this.state.gallery.map((res, i) => {
            indexMap.set(res.id, i);
        });
        this._imageGallery.slideToIndex(indexMap.get(index));
    }

    onNavigate(navUri) {
        let url = navUri == 1 ? root + 'images/' : root + "images/?page=" + navUri;
        client({path: url}).then(img => {
            client({path: root + 'gallery/'}).then(res => {
                this.setState({
                    images: img.entity.results,
                    currentPage: navUri,
                    count: img.entity.count,
                    gallery: res.entity,
                    nbPage: img.entity.count % 2 === 0 ? img.entity.count / 2 : Math.trunc(img.entity.count / 2) + 1
                });
            })
        });
        window.location = "#";
    }

    handleKeyDown (event){
        const P = 80;
        const X = 88;
        const key = parseInt(event.keyCode || event.which || 0);
        var url = new URL(window.location);
        switch (key) {
            case P:
                if(url.hash === '#imageViewer'){
                    this.onUpdate(1);
                }
                break;
            case X:
                if(url.hash === '#imageViewer'){
                    this.onUpdate(2);
                }
                break;
        }
    }

    componentDidMount() {
        this.loadFromServer(this.state.currentPage);
        window.addEventListener('keydown', this.handleKeyDown);
    }

    componentWillUnmount() {
        window.removeEventListener('keydown', this.handleKeyDown);
    }

    render() {
        return (

            <div className="container">
                <CreateDialog onCreate={this.onCreate}/>
                <ImageList images={this.state.images} nbPage={this.state.nbPage} onNavigate={this.onNavigate}
                           currentPage={this.state.currentPage} setIndexViewer={this.setIndexViewer}/>

                <div id="imageViewer" className="modalDialog">
                    <a href="#" title="Close" className="close">X</a>
                    <ImageGallery ref={i => this._imageGallery = i}
                                  items={this.parseData(this.state.gallery)}
                                  showBullets={true}
                                  showIndex={true}
                                  renderItem={this.renderItem}

                    />
                </div>
            </div>
        );
    }
}


export default App;
