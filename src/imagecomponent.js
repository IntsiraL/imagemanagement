import React, {Component} from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {library} from '@fortawesome/fontawesome-svg-core'
import {faCheckCircle, faTimesCircle} from '@fortawesome/free-solid-svg-icons'

library.add(faCheckCircle, faTimesCircle);

class Image extends Component {
    constructor(props) {
        super(props);
        this.handleImg = this.handleImg.bind(this);

    }

    handleImg(e){
        e.preventDefault();
        this.props.setIndexViewer(this.props.image.id);
        window.location = "#imageViewer";
    }

    render() {
        var getName = (url) => {
            let u = new URL(url);
            return u.pathname.split("/").pop();
        };
        var getDate = (date) => {
            let d = new Date(date);
            return d.toString();
        };
        var check = (val) => {
            return val ? <FontAwesomeIcon icon="check-circle" color="green"/> :
                <FontAwesomeIcon icon="times-circle" color="red"/>;
        };
        return (
            <div className="card">
                <img className="card-img-top" src={this.props.image.image}  onClick={this.handleImg}/>
                <div className="card-body">
                    <h4 className="card-title"> {getName(this.props.image.image)} </h4>
                    <p className="card-text"> {getDate(this.props.image.timestamp)} </p>
                    <p className="card-text">Verified {check(this.props.image.verified)}</p>
                    <p className="card-text">Rejected {check(this.props.image.rejected)}</p>
                </div>
            </div>
        );
    }
}

class ImageList extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        var images = this.props.images.map(img => <Image
            key={img.id}
            image={img}
            setIndexViewer={this.props.setIndexViewer}
        />);

        var navLinks = [];
        if (this.props.nbPage >= 2) {
            navLinks.push(<li className="page-item" key="Prev" onClick={()=>this.props.onNavigate(1)}>
                <a className="page-link" href="#" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                    <span className="sr-only">Previous</span>
                </a>
            </li>);
            for (let i = 1; i <= this.props.nbPage; i++) {
                navLinks.push(<li className={i == this.props.currentPage ? "page-item active" : "page-item"} key={i}
                                  onClick={()=>this.props.onNavigate(i)}><a className="page-link" href="#">{i}</a></li>);
            }
            navLinks.push(<li className="page-item" key="Next" onClick={()=>this.props.onNavigate(this.props.nbPage)}>
                <a className="page-link" href="#" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                    <span className="sr-only">Next</span>
                </a>
            </li>);
        }

        return (
            <div className="my-2">
                <h2>Image List</h2>
                <div className="row">
                    {images}
                </div>
                <nav aria-label="PaginationNumber">
                    <ul className="pagination">
                        {navLinks}
                    </ul>
                </nav>
            </div>
        );
    }

}

export default ImageList;