var path = require('path');

module.exports = {
    entry: './src/index.js',
    devtool: 'sourcemaps',
    output: {
        path: __dirname,
        filename: './frontend/static/frontend/bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query: {
                    cacheDirectory: true,
                    presets: ['es2015', '@babel/react']
                }
            },
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            }
        ]
    }
};