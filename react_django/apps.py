from django.apps import AppConfig


class ReactdjangoConfig(AppConfig):
    name = 'react_django'
