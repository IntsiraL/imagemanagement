from rest_framework import serializers
from .models import Image


class ImageSerializer(serializers.HyperlinkedModelSerializer):

    @staticmethod
    def validate_image(value):
        """
            Check that the image dimension is higher than 500x500.
        :param value: image
        :return: image or exception
        """

        if value.image.size[1] < 500:
            raise serializers.ValidationError('Height should be > 500 px.')

        if value.image.size[0] < 500:
            raise serializers.ValidationError('Width should be > 500 px.')

        return value

    class Meta:
        model = Image
        fields = ('id', 'image', 'timestamp', 'verified', 'rejected')
