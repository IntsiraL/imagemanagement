from rest_framework import viewsets, pagination
from .serializers import ImageSerializer
from .models import Image


# Create your views here.

class StandardResultsSetPagination(pagination.PageNumberPagination):
    page_size_query_param = 'page'
    max_page_size = 2
    page_size = 2


class ImageViewset(viewsets.ModelViewSet):
    """
        API endpoint that allows images to be viewed or edited.(with pagination)
    """
    queryset = Image.objects.all().order_by('timestamp')
    serializer_class = ImageSerializer
    pagination_class = StandardResultsSetPagination


class ImageViewsetGallery(viewsets.ModelViewSet):
    """
        API endpoint that allows images to be viewed or edited.
    """
    queryset = Image.objects.all().order_by('timestamp')
    serializer_class = ImageSerializer
