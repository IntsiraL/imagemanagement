from django.contrib import admin
from .models import Image


# Register your models here.

class ImageAdmin(admin.ModelAdmin):
    list_display = ("image", "timestamp", "verified", "rejected")


admin.site.register(Image, ImageAdmin)
