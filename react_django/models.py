from django.db import models
from django.utils.translation import ugettext_lazy as _


class Image(models.Model):
    """
        Modèle pour l'image à stocker
    """

    image = models.ImageField(
        upload_to='images/'
    )

    timestamp = models.DateTimeField(
        auto_now_add=True,
        help_text=_("Upload timestamp for the image.")
    )

    verified = models.BooleanField(
        verbose_name=_("Verified flag"),
        default=False,
        help_text=_("If the image is Kept or Rejected the state of the image should be set to verified=True.")
    )

    rejected = models.BooleanField(
        verbose_name=_("Rejected flag"),
        default=False,
        help_text=_("The state of the image: rejected or not.")
    )

    class Meta:
        verbose_name = _("Image")
        verbose_name_plural = _("Images")

    def __str__(self):
        return " {image} {timestamp}".format(image=self.image.name, timestamp=self.timestamp)
