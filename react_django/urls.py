from django.conf.urls import url, include
from rest_framework import routers
from .views import ImageViewset,ImageViewsetGallery

router = routers.DefaultRouter()
router.register(r'images', ImageViewset)
router.register(r'gallery', ImageViewsetGallery)


# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]